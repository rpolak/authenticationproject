<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
 <head>
    <meta charset="UTF-8">
    <title>Logowanie</title>
    
 </head>
 <body>
 
    <jsp:include page="fragments/_header.jsp"></jsp:include>
    <jsp:include page="fragments/_menu.jsp"></jsp:include>
 <div class="container"> 
    <h3>Strona logowania</h3>
 
    <p style="color: red;">${errorString}</p>
 
    <form method="POST" action="loginDo">
       <table border="0">
          <tr>
             <td>User Name</td>
             <td><input type="text" name="username" value= "${login.username}" /> </td>
          </tr>
          <tr>
             <td>Password</td>
             <td><input type="text" name="password" value= "${login.password}" /> </td>
          </tr>
          <tr>
             <td>Weryfikacja email</td>
             <td><input type="checkbox" id="emailChecker" name="emailValidation" value= "Y" />
             <input type="text" id="email" name="email" value= "${email}" /> </td>
          </tr>
          <tr>
              <td>Weryfikacja sms</td>
              <td><input type="checkbox" id="phoneChecker" name="smsValidation" value="Y" />
              <input type="text" id="phone" name="phone" value= "${phone}" /> </td>
          </tr>
          <tr>
             <td colspan ="2">
                <input type="submit" value= "Zaloguj" />
                <button><a href="${pageContext.request.contextPath}/">Anuluj</a></button>
             </td>
          </tr>
       </table>
    </form>
 
    <p style="color:blue;">User Name: rafal.polak, password: test1test </p>
</div>
    <jsp:include page="fragments/_footer.jsp"></jsp:include>
 
 </body>
</html>