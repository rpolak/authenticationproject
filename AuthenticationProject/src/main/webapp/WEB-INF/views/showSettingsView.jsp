<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
 <head>
    <meta charset="UTF-8">
    <title>Settings</title>
    
 </head>
 <body>
    
    <jsp:include page="fragments/_header.jsp"></jsp:include>
    <jsp:include page="fragments/_menu.jsp"></jsp:include>
<div class="container">
    <h2>Podgląd danych z bazy</h2>
    
    <br />
   <!--Login table-->
   <h3>Login</h3>
   <table width="100%">
       <thead>
       <tr>
           <th>LoginId</th>
           <th>Username</th>
           <th>Password</th>
           <th>Blocked</th>
           <th>IncorrectTry</th>
           <th>BlockedTime</th>
           <th>CreatedAt</th>
       </tr>
       </thead>
       <tbody>
        <c:forEach items="${loginsArray}" var="item">
            <tr>
                <td><c:out value="${item.getLoginId()}" /></td>
                <td><c:out value="${item.getUsername()}" /></td>
                <td><c:out value="${item.getPassword()}" /></td>
                <td><c:out value="${item.getBlocked()}" /></td>
                <td><c:out value="${item.getIncorrectTry()}" /></td>
                <td><c:out value="${item.getBlockedTime()}" /></td>
                <td><c:out value="${item.getCreatedAt()}" /></td>
            </tr>
        </c:forEach>
        </tbody>
   </table>
   <br />
   
   <!--User table-->
   <h3>Użytkowinik</h3>
   <table width="100%">
       <tr>
           <th>UserId</th>
           <th>LoginId</th>
           <th>Name</th>
           <th>Surname</th>
           <th>Address</th>
           <th>Email</th>
           <th>Phone</th>
           <th>CreatedAt</th>
       </tr>
        <c:forEach items="${userArray}" var="item">
            <tr>
                <td><c:out value="${item.getUserId()}" /></td>
                <td><c:out value="${item.getLoginId()}" /></td>
                <td><c:out value="${item.getName()}" /></td>
                <td><c:out value="${item.getSurname()}" /></td>
                <td><c:out value="${item.getAddress()}" /></td>
                <td><c:out value="${item.getEmail()}" /></td>
                <td><c:out value="${item.getPhone()}" /></td>
                <td><c:out value="${item.getCreatedAt()}" /></td>
            </tr>
        </c:forEach>
   </table>
   <br />
      
   <!--Historia haseł table-->
   <h3>Informacje o Historii haseł</h3>
   <table width="100%">
       <tr>
           <th>PasswordHistoryId</th>
           <th>LoginId</th>
           <th>Password</th>
           <th>CreatedAt</th>
       </tr>
        <c:forEach items="${passwordHistoryArray}" var="item">
            <tr>
                <td><c:out value="${item.getPasswordHistoryId()}" /></td>
                <td><c:out value="${item.getLoginId()}" /></td>
                <td><c:out value="${item.getPassword()}" /></td>
                <td><c:out value="${item.getCreatedAt()}" /></td>
            </tr>
        </c:forEach>
   </table>
   
   
   <br />
   <!--Informacja o zalogowaniu table-->
   <h3>Informacje o zalogowaniu</h3>
   <table width="100%">
       <tr>
           <th>LoggedInfoId</th>
           <th>LoginId</th>
           <th>LoggedInTime</th>
           <th>LoggedOutTime</th>
           <th>CreatedAt</th>
       </tr>
        <c:forEach items="${loggedInfosArray}" var="item">
            <tr>
                <td><c:out value="${item.getLoggedInfoId()}" /></td>
                <td><c:out value="${item.getLoginId()}" /></td>
                <td><c:out value="${item.getLoggedInTime()}" /></td>
                <td><c:out value="${item.getLoggedOutTime()}" /></td>
                <td><c:out value="${item.getCreatedAt()}" /></td>
            </tr>
        </c:forEach>
   </table>
   
   <br />
   <!--Weryfikacja table-->
   <h3>Weryfikacja danych</h3>
   <table width="100%">
       <tr>
           <th>VerificationId</th>
           <th>LoginId</th>
           <th>SmsVerification</th>
           <th>EmailVerification</th>
           <th>CreatedAt</th>
       </tr>
        <c:forEach items="${verificationArray}" var="item">
            <tr>
                <td><c:out value="${item.getVerificationId()}" /></td>
                <td><c:out value="${item.getLoginId()}" /></td>
                <td><c:out value="${item.getSmsVerification()}" /></td>
                <td><c:out value="${item.getEmailVerification()}" /></td>
                <td><c:out value="${item.getCreatedAt()}" /></td>
            </tr>
        </c:forEach>
   </table>
  </div>
     <jsp:include page="fragments/_footer.jsp"></jsp:include>
    
 </body>

</html>