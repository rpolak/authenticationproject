<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Zmiana hasła</title>
    </head>
    <body>
        
    <jsp:include page="fragments/_header.jsp"></jsp:include>
    <jsp:include page="fragments/_menu.jsp"></jsp:include>
<div class="container">  
    <h3>Zmiana hasła</h3>
    
    <p style="color: red;">${errorString}</p>
        
        <form method="POST" action="changePasswordDo">
       <table border="0">
          <tr>
             <td>Nowe hasło</td>
             <td><input type="text" name="newPassword" value= "${newPassword}" /> </td>
          </tr>
          <tr>
             <td>Nowe hasło ponownie</td>
             <td><input type="text" name="newPasswordAgain" value= "${newPasswordAgain}" /> </td>
          </tr>
          <tr>
             <td colspan ="2">
                <input type="submit" value= "Zmień hasło" />
             </td>
          </tr>
       </table>
    </form>
</div>    
    <jsp:include page="fragments/_footer.jsp"></jsp:include>
             
    </body>
</html>
