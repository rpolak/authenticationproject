<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<div style="padding: 5px;">
 
   <a href="${pageContext.request.contextPath}/">Home</a>
   |
   <a href="${pageContext.request.contextPath}/userInfo">Dane użytkownika</a>
   |
   <a href="${pageContext.request.contextPath}/showSettings">Podgląd danych w bazie</a>
   <c:choose>
    <c:when test="${isLogged == true}">
        |
        <a id="changePassword" href="${pageContext.request.contextPath}/changePassword">Zmień hasło</a>
        |
        <a id="logout" href="${pageContext.request.contextPath}/logout">Wyloguj</a>
    </c:when>
    <c:when test="${isLogged == false}">
        |
        <a id="login" href="${pageContext.request.contextPath}/login">Logowanie</a>
    </c:when>
    <c:otherwise>
    </c:otherwise>
   </c:choose>
</div>  