<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
     <meta charset="UTF-8">
     <title>Home Page</title>
  </head>
  <body>
 
     <jsp:include page="fragments/_header.jsp"></jsp:include>
     <jsp:include page="fragments/_menu.jsp"></jsp:include>
<div class="container"> 
      <h3>Strona główna</h3>
      
      Aplikacja realizująca logowanie wraz z informacjami <br><br>
      <b>Zawiera:</b>
      <ul>
         <li>Logowanie</li>
         <li>Zliczanie ilości niepoprawnych logowań i blokowanie na 15 minut po 3 błędnych próbach</li>
         <li>Rejestrowanie 3 ostatnich haseł w bazie, przy zmianie hasła sprawdzanie czy powtarza się w 3 ostatnio zmienianych</li>
         <li>Weryfikacja po telefonie lub e-mailu przy logowaniu po zaznaczeniu opcji</li>
         <li>Dane użytkownika</li>
         <li>Podgląd danych w bazie</li>
         <li>Zmiana hasła po zalogowaniu</li>
         <li>Dodawanie danych do bazy przy weryfikacji lub logowaniu/wylogowywaniu użytkownika</li>
         <li>Wylogowywanie</li>
      </ul>
</div>
     <jsp:include page="fragments/_footer.jsp"></jsp:include>
 
  </body>
</html>