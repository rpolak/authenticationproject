<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
 <head>
    <meta charset="UTF-8">
    <title>User Info</title>
    
 </head>
 <body>
    
    <jsp:include page="fragments/_header.jsp"></jsp:include>
    <jsp:include page="fragments/_menu.jsp"></jsp:include>
<div class="container"> 
    <h2>Dane użytkownika</h2>
    
    <br />
    Login: ${user.getUsername()} <br />
    Czy Zablokowany: ${user.getBlocked()} <br/>
    Ilość niepoprawnych logowań: ${user.getIncorrectTry()} <br/>
</div>
     <jsp:include page="fragments/_footer.jsp"></jsp:include>
 
 </body>
</html>