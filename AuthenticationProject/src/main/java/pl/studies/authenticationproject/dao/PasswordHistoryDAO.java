package pl.studies.authenticationproject.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import pl.studies.authenticationproject.entity.PasswordHistory;

public class PasswordHistoryDAO {

    public static ArrayList<PasswordHistory> returnDataFromPasswordHistoryTable(Connection connection) throws SQLException {
        String sql = "SELECT * FROM passwordhistory";

        PreparedStatement pstm = connection.prepareStatement(sql);
        ResultSet resultSet = pstm.executeQuery();

        ArrayList<PasswordHistory> passwordHistorys = new ArrayList<>();

        while (resultSet.next()) {
            PasswordHistory passwordHistory = new PasswordHistory(resultSet.getLong("passwordHistoryId"),
                    resultSet.getLong("loginId"),
                    resultSet.getString("password"),
                    resultSet.getTimestamp("createdAt"));
            passwordHistorys.add(passwordHistory);
        }

        return passwordHistorys;
    }

    public static ArrayList<String> checkPasswordHistory(Connection connection, Long loginId) throws SQLException {

        String sql = "SELECT password FROM passwordhistory "
                + "WHERE loginId = ? "
                + "ORDER BY createdAt DESC "
                + "LIMIT 3";

        String password = null;

        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setLong(1, loginId);
        
        ResultSet resultSet = preparedStatement.executeQuery();

        ArrayList<String> passwords = new ArrayList<>();

        while (resultSet.next()) {
            password = resultSet.getString("password");
            passwords.add(password);
        }
        return passwords;
    }
    
    public static void insertNewPasswordHistory(Connection connection, Long loginId, String password) throws SQLException {
        
           String sql = "INSERT INTO passwordhistory "
                + "(loginId, password, createdAt) VALUES "
                + "(?,?,NOW())";

        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setLong(1, loginId);
        preparedStatement.setString(2, password);

        // execute insert SQL stetement
        preparedStatement.execute();                
    }

}
