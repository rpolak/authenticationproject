package pl.studies.authenticationproject.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import pl.studies.authenticationproject.entity.User;

public class UserDAO {

    public static ArrayList<User> returnDataFromUserTable(Connection connection) throws SQLException {
        String sql = "SELECT * FROM user";

        PreparedStatement pstm = connection.prepareStatement(sql);
        ResultSet resultSet = pstm.executeQuery();

        ArrayList<User> users = new ArrayList<>();

        while (resultSet.next()) {
            User user = new User(resultSet.getLong("userId"),
                    resultSet.getLong("loginId"),
                    resultSet.getString("name"),
                    resultSet.getString("surname"),
                    resultSet.getString("address"),
                    resultSet.getString("email"),
                    resultSet.getString("phone"),
                    resultSet.getTimestamp("createdAt"));
            users.add(user);
        }

        return users;
    }

    public static void changeEmailAddressUser(Connection connection, Long loginId, String email) throws SQLException {
        String sql = "UPDATE user "
                   + "SET email = ? "
                   + "WHERE loginId = ?";
        
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, email);
        preparedStatement.setLong(2, loginId);
        
        preparedStatement.executeUpdate();
        
    }
    
      public static User findUserInformation(Connection connection, String username) throws SQLException {
 
      String sql = "SELECT u.userId, u.loginId, u.name, u.surname, u.address, u.email, u.phone FROM user u " +
                   "LEFT JOIN login l " +
                   "ON u.loginId = l.loginId " +
                   "WHERE l.username = ?";
 
      PreparedStatement pstm = connection.prepareStatement(sql);
      pstm.setString(1, username);
 
      ResultSet resultSet = pstm.executeQuery();
 
      if (resultSet.next()) {
          Long userId = resultSet.getLong("userId");
          Long loginId = resultSet.getLong("loginId");
          String name = resultSet.getString("name");
          String surname = resultSet.getString("surname");
          String address = resultSet.getString("address");
          String email = resultSet.getString("email");
          String phone = resultSet.getString("phone");

          User user = new User();
          user.setUserId(userId);
          user.setLoginId(loginId);
          user.setName(name);
          user.setSurname(surname);
          user.setAddress(address);
          user.setEmail(email);
          user.setPhone(phone);
          
          return user;
      }
      return null;
  }
    
}
