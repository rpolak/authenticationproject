package pl.studies.authenticationproject.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import pl.studies.authenticationproject.entity.Login;

public class LoginDAO {

    //sprawdzenie danych użytkownika
    public static Login checkUserCredentials(Connection connection, String username, String password) throws SQLException {
 
      String sql = "SELECT l.loginId, l.username, l.password, l.blocked, l.incorrectTry FROM Login l "
              + " WHERE l.username = ? AND l.password= ?";
 
      PreparedStatement pstm = connection.prepareStatement(sql);
      pstm.setString(1, username);
      pstm.setString(2, password);
        ResultSet resultSet = pstm.executeQuery();
 
      if (resultSet.next()) {
          Long loginId = resultSet.getLong("loginId");
          Boolean blocked = resultSet.getBoolean("blocked");
          int incorrectTry = resultSet.getInt("incorrectTry");

          Login login = new Login();
          login.setLoginId(loginId);
          login.setUsername(username);
          login.setPassword(password);
          login.setBlocked(blocked);
          login.setIncorrectTry(incorrectTry);

          return login;
      }
      return null;
  }
     
  public static ArrayList<Login> returnDataFromLoginTable(Connection connection) throws SQLException{
      String sql = "SELECT * FROM login";
      
      PreparedStatement pstm = connection.prepareStatement(sql);
      ResultSet resultSet = pstm.executeQuery();
      
      ArrayList<Login> logins = new ArrayList<>();
      
      
      while(resultSet.next()){          
          Login login = new Login(resultSet.getLong("loginId"),
                                  resultSet.getString("username"),
                                  resultSet.getString("password"),
                                  resultSet.getBoolean("blocked"),
                                  resultSet.getInt("incorrectTry"),
                                  resultSet.getTimestamp("blockedTime"),
                                  resultSet.getTimestamp("createdAt"));
        logins.add(login);
      }
      
      return logins;
     
  }
  
  //metoda zmiany hasła użytkownika
  public static void changeUserPassword(Connection connection, Long loginId, String password) throws SQLException {
        String sql = "UPDATE login "
                   + "SET password = ? "
                   + "WHERE loginId = ?";
        
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, password);
        preparedStatement.setLong(2, loginId);
        
        preparedStatement.executeUpdate();
        
    }
  
  //metoda do dodawania niepoprawnego logowania
   public static void addIncorrectTry(Connection connection, Long loginId)throws SQLException {
      
      int incorrectTry = 0;
      
      String fetchDataSql = "SELECT incorrectTry "
                          + "FROM login "
                          + "WHERE loginId = ?";
      
      PreparedStatement preparedStatement = connection.prepareStatement(fetchDataSql);
      preparedStatement.setLong(1, loginId);
      
      ResultSet resultSet = preparedStatement.executeQuery();
      
      while(resultSet.next()){
         incorrectTry = resultSet.getInt("incorrectTry");
      }
      
      String updateIncorrectTry = "UPDATE login "
                                + "SET incorrectTry = ? "
                                + "WHERE loginId = ?";
      
        preparedStatement = connection.prepareStatement(updateIncorrectTry);
        preparedStatement.setInt(1, ++incorrectTry);
        preparedStatement.setLong(2, loginId);
        
        preparedStatement.executeUpdate();
  }
   
   
  //metoda do ustawienia blokady na true jeśli przekroczono 3 logowania
   public static void setBlocked(Connection connection, Long loginId) throws SQLException {
       
       String sql = "UPDATE login "
                  + "SET blockedTime = "
                  + "CASE WHEN incorrectTry >= 3 THEN NOW() "
                  + "ELSE blockedTime END, "
                  + "blocked = "
                  + "CASE WHEN incorrectTry >= 3 THEN 1 "
                  + "ELSE blocked "
                  + "END "
                  + "WHERE loginId = ?";
       
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setLong(1, loginId);
        
        preparedStatement.executeUpdate();
               
   }
  
  //metoda sprawdzająca czy minęło 15 minut i wtedy odblokowanie użytkownika
   public static boolean isUnblocked(Connection connection, Long loginId) throws SQLException {
       
       String unblocked = "UPDATE login "
                  + "SET blocked = "
                  + "CASE WHEN TIMESTAMPDIFF(MINUTE,blockedTime,NOW()) > 15 THEN 0 "
                  + "ELSE blocked END, "
                  + "incorrectTry = "
                  + "CASE WHEN TIMESTAMPDIFF(MINUTE,blockedTime,NOW()) > 15 THEN 0 "
                  + "ELSE incorrectTry END, "
                  + "blockedTime = "
                  + "CASE WHEN TIMESTAMPDIFF(MINUTE,blockedTime,NOW()) > 15 THEN NULL "
                  + "ELSE blockedTime "
                  + "END "
                  + "WHERE loginId = ?";       
       
       
       String isBlocked = null;
       
       PreparedStatement preparedStatement = connection.prepareStatement(unblocked);
       preparedStatement.setLong(1, loginId);
       
       preparedStatement.executeUpdate();
       
       String checkIfUnblocked = "SELECT if(blocked = 1, 'true', 'false') AS decision "
               + "FROM login "
               + "WHERE loginId = ?";
       
       preparedStatement = connection.prepareStatement(checkIfUnblocked);
       preparedStatement.setLong(1, loginId);
       
       ResultSet resultSet = preparedStatement.executeQuery();
       
       while(resultSet.next()){
         isBlocked = resultSet.getString("decision");
      }
       
       return isBlocked.toLowerCase().contains("true");
   }
   
  
}
