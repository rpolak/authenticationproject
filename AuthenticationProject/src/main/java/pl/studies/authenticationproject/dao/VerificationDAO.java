package pl.studies.authenticationproject.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import pl.studies.authenticationproject.entity.Verification;

public class VerificationDAO {

    public static ArrayList<Verification> returnDataFromVerificationTable(Connection connection) throws SQLException {
        String sql = "SELECT * FROM verification";

        PreparedStatement pstm = connection.prepareStatement(sql);
        ResultSet resultSet = pstm.executeQuery();

        ArrayList<Verification> verifications = new ArrayList<>();

        while (resultSet.next()) {
            Verification verification = new Verification(resultSet.getLong("verificationId"),
                    resultSet.getLong("loginId"),
                    resultSet.getBoolean("smsVerification"),
                    resultSet.getBoolean("emailVerification"),
                    resultSet.getTimestamp("createdAt"));
            verifications.add(verification);
        }

        return verifications;
    }

    public static void addVerificationSMS(Connection connection, Long loginId) throws SQLException {

        String sql = "INSERT INTO verification "
                + "(loginId, smsVerification, emailVerification, createdAt) "
                + "VALUES (?,true,false,NOW())";

        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setLong(1, loginId);

        preparedStatement.execute();
    }

    public static void addVerificationEmail(Connection connection, Long loginId) throws SQLException {

        String sql = "INSERT INTO verification "
                + "(loginId, smsVerification, emailVerification, createdAt) "
                + "VALUES (?,false,true,NOW())";

        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setLong(1, loginId);

        preparedStatement.execute();
    }

}
