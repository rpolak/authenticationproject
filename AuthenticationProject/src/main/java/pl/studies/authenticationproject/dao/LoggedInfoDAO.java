package pl.studies.authenticationproject.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import pl.studies.authenticationproject.entity.LoggedInfo;

public class LoggedInfoDAO {

    public static ArrayList<LoggedInfo> returnDataFromLoggedInfoTable(Connection connection) throws SQLException {
        String sql = "SELECT * FROM loggedInfo";

        PreparedStatement pstm = connection.prepareStatement(sql);
        ResultSet resultSet = pstm.executeQuery();

        ArrayList<LoggedInfo> loggedInfos = new ArrayList<>();

        while (resultSet.next()) {
            LoggedInfo loggedInfo = new LoggedInfo(resultSet.getLong("loggedInfoId"),
                    resultSet.getLong("loginId"),
                    resultSet.getTimestamp("loggedInTime"),
                    resultSet.getTimestamp("loggedOutTime"),
                    resultSet.getTimestamp("createdAt"));
            loggedInfos.add(loggedInfo);
        }

        return loggedInfos;
    }

    public static void userLoggedInCreateRecord(Connection connection, Long loginId) throws SQLException {

        String sql = "INSERT INTO loggedinfo "
                + "(loginId, loggedInTime, loggedOutTime, createdAt) VALUES "
                + "(?,NOW(),null,NOW())";

        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setLong(1, loginId);

        preparedStatement.execute();

    }

    public static void userLoggedOutUpdateRecord(Connection connection, Long loginId) throws SQLException {
        String sql = "UPDATE loggedinfo "
                + "SET loggedOutTime = NOW() "
                + "WHERE loginId = ?";
        
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setLong(1, loginId);
        
        preparedStatement.executeUpdate();
    }

}
