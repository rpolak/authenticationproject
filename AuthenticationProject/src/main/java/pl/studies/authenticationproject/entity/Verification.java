package pl.studies.authenticationproject.entity;

import java.util.Date;

public class Verification {

    private Long verificationId;
    private Long loginId;
    private Boolean smsVerification;
    private Boolean emailVerification;
    private Date createdAt;

    public Verification() {
    }

    public Verification(Long verificationId, Long loginId, Boolean smsVerification, Boolean emailVerification, Date createdAt) {
        this.verificationId = verificationId;
        this.loginId = loginId;
        this.smsVerification = smsVerification;
        this.emailVerification = emailVerification;
        this.createdAt = createdAt;
    }

    public Long getVerificationId() {
        return verificationId;
    }

    public void setVerificationId(Long verificationId) {
        this.verificationId = verificationId;
    }

    public Long getLoginId() {
        return loginId;
    }

    public void setLoginId(Long loginId) {
        this.loginId = loginId;
    }

    public Boolean getSmsVerification() {
        return smsVerification;
    }

    public void setSmsVerification(Boolean smsVerification) {
        this.smsVerification = smsVerification;
    }

    public Boolean getEmailVerification() {
        return emailVerification;
    }

    public void setEmailVerification(Boolean emailVerification) {
        this.emailVerification = emailVerification;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
