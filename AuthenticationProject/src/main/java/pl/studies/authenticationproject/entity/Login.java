package pl.studies.authenticationproject.entity;

import java.util.Date;

public class Login {

    private Long loginId;
    private String username;
    private String password;
    private Boolean blocked;
    private int incorrectTry;
    private Date blockedTime;
    private Date createdAt;

    public Login() {
    }

    public Login(Long loginId, String username, String password, Boolean blocked, int incorrectTry, Date blockedTime, Date createdAt) {
        this.loginId = loginId;
        this.username = username;
        this.password = password;
        this.blocked = blocked;
        this.incorrectTry = incorrectTry;
        this.blockedTime = blockedTime;
        this.createdAt = createdAt;
    }

    public Long getLoginId() {
        return loginId;
    }

    public void setLoginId(Long loginId) {
        this.loginId = loginId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getBlocked() {
        return blocked;
    }

    public void setBlocked(Boolean blocked) {
        this.blocked = blocked;
    }

    public int getIncorrectTry() {
        return incorrectTry;
    }

    public void setIncorrectTry(int incorrectTry) {
        this.incorrectTry = incorrectTry;
    }

    public Date getBlockedTime() {
        return blockedTime;
    }

    public void setBlockedTime(Date blockedTime) {
        this.blockedTime = blockedTime;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
