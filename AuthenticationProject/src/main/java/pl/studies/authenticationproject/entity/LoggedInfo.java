package pl.studies.authenticationproject.entity;

import java.util.Date;

public class LoggedInfo {

    private Long loggedInfoId;
    private Long loginId;
    private Date loggedInTime;
    private Date loggedOutTime;
    private Date createdAt;

    public LoggedInfo() {
    }

    public LoggedInfo(Long loggedInfoId, Long loginId, Date loggedInTime, Date loggedOutTime, Date createdAt) {
        this.loggedInfoId = loggedInfoId;
        this.loginId = loginId;
        this.loggedInTime = loggedInTime;
        this.loggedOutTime = loggedOutTime;
        this.createdAt = createdAt;
    }

    public Long getLoggedInfoId() {
        return loggedInfoId;
    }

    public void setLoggedInfoId(Long loggedInfoId) {
        this.loggedInfoId = loggedInfoId;
    }

    public Long getLoginId() {
        return loginId;
    }

    public void setLoginId(Long loginId) {
        this.loginId = loginId;
    }

    public Date getLoggedInTime() {
        return loggedInTime;
    }

    public void setLoggedInTime(Date loggedInTime) {
        this.loggedInTime = loggedInTime;
    }

    public Date getLoggedOutTime() {
        return loggedOutTime;
    }

    public void setLoggedOutTime(Date loggedOutTime) {
        this.loggedOutTime = loggedOutTime;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
