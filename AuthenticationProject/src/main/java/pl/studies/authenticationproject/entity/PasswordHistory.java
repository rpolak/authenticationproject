package pl.studies.authenticationproject.entity;

import java.util.Date;

public class PasswordHistory {

    private Long passwordHistoryId;
    private Long loginId;
    private String password;
    private Date createdAt;

    public PasswordHistory() {
    }

    public PasswordHistory(Long passwordHistoryId, Long loginId, String password, Date createdAt) {
        this.passwordHistoryId = passwordHistoryId;
        this.loginId = loginId;
        this.password = password;
        this.createdAt = createdAt;
    }

    public Long getPasswordHistoryId() {
        return passwordHistoryId;
    }

    public void setPasswordHistoryId(Long passwordHistoryId) {
        this.passwordHistoryId = passwordHistoryId;
    }

    public Long getLoginId() {
        return loginId;
    }

    public void setLoginId(Long loginId) {
        this.loginId = loginId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
