package pl.studies.authenticationproject.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import pl.studies.authenticationproject.dao.LoginDAO;
import pl.studies.authenticationproject.dao.PasswordHistoryDAO;

import pl.studies.authenticationproject.util.FrontUtil;
import pl.studies.authenticationproject.entity.Login;
import pl.studies.authenticationproject.util.MySQLConnection;

@WebServlet(urlPatterns = {"/changePasswordDo"})
public class ChangePasswordDo extends HttpServlet {

    private final String ERROR_NOT_EQUAL = "Podane hasła nie są sobie równe";
    private final String ERROR_PASS_ALREADY_USED = "Hasło już było wcześniej użyte";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        Connection connection = null;

        String newPassword = request.getParameter("newPassword");
        String newPasswordAgain = request.getParameter("newPasswordAgain");
        ArrayList<String> passwords = new ArrayList<>();

        boolean hasError = false;
        String errorString = null;

        // Check User has logged on
        Login loginedUser = FrontUtil.getLoginedUser(session);

        // Not logged in
        if (loginedUser == null) {

            // Redirect to login page.
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }

        //check if passwords are equals
        if (!newPassword.equals(newPasswordAgain)) {
            hasError = true;
            errorString = ERROR_NOT_EQUAL;
            
            request.setAttribute("errorString", errorString);
            
            RequestDispatcher dispatcher
                    = this.getServletContext().getRequestDispatcher("/changePassword");

            dispatcher.forward(request, response);
        }

        //check if new password's is already been used
        try {
            connection = MySQLConnection.getMySQLConnection();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(LoginDo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(LoginDo.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            passwords = PasswordHistoryDAO.checkPasswordHistory(connection, loginedUser.getLoginId());

        } catch (SQLException e) {
            e.printStackTrace();
            hasError = true;
            errorString = e.getMessage();
        }
        

        if (FrontUtil.checkIfPasswordsIsInList(newPassword, passwords)) {
            hasError = true;
            errorString = ERROR_PASS_ALREADY_USED;
            
            request.setAttribute("errorString", errorString);
            
            RequestDispatcher dispatcher
                    = this.getServletContext().getRequestDispatcher("/changePassword");

            dispatcher.forward(request, response);
        }

        //update password and insert value in password history table
        if (!hasError) {
            try {
                connection = MySQLConnection.getMySQLConnection();
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(LoginDo.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(LoginDo.class.getName()).log(Level.SEVERE, null, ex);
            }

            try {
                LoginDAO.changeUserPassword(connection, loginedUser.getLoginId(), newPassword);
                PasswordHistoryDAO.insertNewPasswordHistory(connection, loginedUser.getLoginId(), loginedUser.getPassword());

            } catch (SQLException e) {
                e.printStackTrace();
                hasError = true;
                errorString = e.getMessage();
            }
        }

        //if error redirect to login
        if (hasError) {

            // Store information in request attribute, before forward.
            request.setAttribute("errorString", errorString);

            // Forward to /WEB-INF/views/login.jsp
            RequestDispatcher dispatcher
                    = this.getServletContext().getRequestDispatcher("/login");

            dispatcher.forward(request, response);
        } else {

            FrontUtil.closeSession(session);
            
            // Redirect to login page.
            response.sendRedirect(request.getContextPath() + "/login");
        }

    }

}
