package pl.studies.authenticationproject.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import pl.studies.authenticationproject.dao.LoggedInfoDAO;
import pl.studies.authenticationproject.dao.LoginDAO;
import pl.studies.authenticationproject.dao.UserDAO;
import pl.studies.authenticationproject.dao.VerificationDAO;
import pl.studies.authenticationproject.entity.Login;
import pl.studies.authenticationproject.entity.User;
import pl.studies.authenticationproject.util.FrontUtil;
import pl.studies.authenticationproject.util.MySQLConnection;

@WebServlet(urlPatterns = "/loginDo")
public class LoginDo extends HttpServlet {

   @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
 
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        
        String emailField = request.getParameter("emailValidation");
        boolean emailValidation = "Y".equals(emailField);
        
        String smsField = request.getParameter("smsValidation");
        boolean smsValidation = "Y".equals(smsField);
 
        boolean isBlocked = false;
         
        Login login = null;
        User user = null;
        boolean hasError = false;
        String errorString = null;
        
        Connection connection = null;
 
        if (username == null || password == null
                 || username.length() == 0 || password.length() == 0) {
            hasError = true;
            errorString = "login i hasło są wymagane!";
        } else {
            try {
                connection = MySQLConnection.getMySQLConnection();
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(LoginDo.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(LoginDo.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            try {
              
                user = UserDAO.findUserInformation(connection, username);
                if(user!=null){
                isBlocked = LoginDAO.isUnblocked(connection, user.getLoginId());
                
                
                if(!isBlocked){
                
                login = LoginDAO.checkUserCredentials(connection, username, password);
                
                //check verification if selected
                if(smsValidation){
                    if(!user.getPhone().equals(phone)){
                        hasError = true;
                        errorString = "weryfikacja sms'owa nie powiodła się";
                    }else{
                        VerificationDAO.addVerificationSMS(connection, login.getLoginId());
                    }
                }
                
                if(emailValidation){
                    if(!user.getEmail().equals(email)){
                        hasError = true;
                        errorString = "weryfikacja email'owa nie powiodła się";
                    }else{
                        VerificationDAO.addVerificationEmail(connection, login.getLoginId());
                    }
                }
                }
                
                if (login == null) {
                    hasError = true;
                    errorString = "login lub hasło jest niepoprawne";
                    
                    
                    //check if user is blocked/unblocked/add incorrectTry
                    if(user != null){
                        LoginDAO.addIncorrectTry(connection, user.getLoginId());
                        LoginDAO.setBlocked(connection, user.getLoginId());
                    }
                    
                }else{
                    LoggedInfoDAO.userLoggedInCreateRecord(connection, login.getLoginId());
                }
                }
                
            } catch (SQLException e) {
                e.printStackTrace();
                hasError = true;
                errorString = e.getMessage();
            }
        }
        
        
        if(isBlocked){
                    hasError = true;
                    errorString = "konto zablokowane, odczekaj 15 minut!!!";
        }
        
        // If error, forward to /WEB-INF/views/login.jsp
        if (hasError) {
            login = new Login();
            login.setUsername(username);
            login.setPassword(password);
        
            // Store information in request attribute, before forward.
            request.setAttribute("errorString", errorString);
            request.setAttribute("user", login);
 
       
            // Forward to /WEB-INF/views/login.jsp
            RequestDispatcher dispatcher //
            = this.getServletContext().getRequestDispatcher("/WEB-INF/views/loginView.jsp");
 
            dispatcher.forward(request, response);
        }
     
        // If no error
        // Store user information in Session
        // And redirect to userInfo page.
        else {
            HttpSession session = request.getSession();
            FrontUtil.storeLoginedUser(session, login);
                   
            // Redirect to userInfo page.
            response.sendRedirect(request.getContextPath() + "/userInfo");
        }
    }
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

}
