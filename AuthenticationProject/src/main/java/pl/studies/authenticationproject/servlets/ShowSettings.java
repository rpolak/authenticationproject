package pl.studies.authenticationproject.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import pl.studies.authenticationproject.dao.LoggedInfoDAO;
import pl.studies.authenticationproject.dao.LoginDAO;
import pl.studies.authenticationproject.dao.PasswordHistoryDAO;
import pl.studies.authenticationproject.dao.UserDAO;
import pl.studies.authenticationproject.dao.VerificationDAO;
import pl.studies.authenticationproject.entity.LoggedInfo;

import pl.studies.authenticationproject.util.FrontUtil;
import pl.studies.authenticationproject.entity.Login;
import pl.studies.authenticationproject.entity.PasswordHistory;
import pl.studies.authenticationproject.entity.User;
import pl.studies.authenticationproject.entity.Verification;
import pl.studies.authenticationproject.util.MySQLConnection;

@WebServlet(urlPatterns = {"/showSettings"})
public class ShowSettings extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ArrayList<Login> logins = new ArrayList<>();
        ArrayList<LoggedInfo> loggedInfos = new ArrayList<>();
        ArrayList<PasswordHistory> passwordHistorys = new ArrayList<>();
        ArrayList<User> users = new ArrayList<>();
        ArrayList<Verification> verifications = new ArrayList<>();
        
        boolean hasError = false;
        String errorString = null;
        
        HttpSession session = request.getSession();

        // Check User has logged on
        Login loginedUser = FrontUtil.getLoginedUser(session);

        // Not logged in
        if (loginedUser == null) {

            // Redirect to login page.
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }

        // Store info in request attribute
        request.setAttribute("user", loginedUser);
        
        
        // Retrieve data from database
        Connection connection = null;
            try {
                connection = MySQLConnection.getMySQLConnection();
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(LoginDo.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(LoginDo.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        try {
                logins = LoginDAO.returnDataFromLoginTable(connection);
                loggedInfos = LoggedInfoDAO.returnDataFromLoggedInfoTable(connection);
                passwordHistorys = PasswordHistoryDAO.returnDataFromPasswordHistoryTable(connection);
                users = UserDAO.returnDataFromUserTable(connection);
                verifications = VerificationDAO.returnDataFromVerificationTable(connection);
                
                if (logins == null) {
                    hasError = true;
                    errorString = "Empty results";
                }
            } catch (SQLException e) {
                e.printStackTrace();
                hasError = true;
                errorString = e.getMessage();
            }
        
        request.setAttribute("loginsArray", logins);
        request.setAttribute("loggedInfosArray", loggedInfos);
        request.setAttribute("passwordHistoryArray", passwordHistorys);
        request.setAttribute("userArray", users);
        request.setAttribute("verificationArray", verifications);
        
        
        // Logined, forward to /WEB-INF/views/showSettingsView.jsp
        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/showSettingsView.jsp");
        dispatcher.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

}
