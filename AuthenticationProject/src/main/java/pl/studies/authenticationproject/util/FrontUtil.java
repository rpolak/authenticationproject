package pl.studies.authenticationproject.util;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import pl.studies.authenticationproject.dao.LoggedInfoDAO;
import pl.studies.authenticationproject.entity.Login;
import pl.studies.authenticationproject.servlets.LoginDo;

public class FrontUtil {

    // Store user info in Session.
    public static void storeLoginedUser(HttpSession session, Login loginedUser) {

        // On the JSP can access ${loginedUser}
        session.setAttribute("loginedUser", loginedUser);
        session.setAttribute("isLogged", true);
    }

    // Get the user information stored in the session.
    public static Login getLoginedUser(HttpSession session) {
        Login loginedUser = (Login) session.getAttribute("loginedUser");

        return loginedUser;
    }

    // Remove attribute's
    public static void closeSession(HttpSession session) {

        session.setAttribute("isLogged", false);

        Connection connection = null;
        try {
            connection = MySQLConnection.getMySQLConnection();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(LoginDo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(LoginDo.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            LoggedInfoDAO.userLoggedOutUpdateRecord(connection, FrontUtil.getLoginedUser(session).getLoginId());

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static boolean checkIfPasswordsIsInList(String password, ArrayList<String> passwords) {
        return passwords.contains(password);
    }

}
