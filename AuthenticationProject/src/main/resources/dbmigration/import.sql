CREATE SCHEMA `authenticationproject` DEFAULT CHARACTER SET utf8 COLLATE utf8_polish_ci ;

CREATE TABLE `authenticationproject`.`user` (
  `userId` BIGINT NOT NULL AUTO_INCREMENT,
  `loginId` BIGINT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `surname` VARCHAR(45) NOT NULL,
  `address` VARCHAR(100) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `phone` VARCHAR(45) NOT NULL,
  `createdAt` DATETIME NOT NULL,
  PRIMARY KEY (`userId`));

CREATE TABLE `authenticationproject`.`login` (
  `loginId` BIGINT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `blocked` TINYINT NOT NULL,
  `incorrectTry` INT NOT NULL,
  `blockedTime` DATETIME NULL,
  `createdAt` DATETIME NOT NULL,
  PRIMARY KEY (`loginId`));

CREATE TABLE `authenticationproject`.`passwordhistory` (
  `passwordHistoryId` BIGINT NOT NULL AUTO_INCREMENT,
  `loginId` BIGINT NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `createdAt` DATETIME NOT NULL,
  PRIMARY KEY (`passwordHistoryId`));

CREATE TABLE `authenticationproject`.`verification` (
  `verificationId` BIGINT NOT NULL AUTO_INCREMENT,
  `loginId` BIGINT NOT NULL,
  `smsVerification` TINYINT NOT NULL,
  `emailVerification` TINYINT NOT NULL,
  `createdAt` DATETIME NOT NULL,
  PRIMARY KEY (`verificationId`));

CREATE TABLE `authenticationproject`.`loggedinfo` (
  `loggedInfoId` BIGINT NOT NULL AUTO_INCREMENT,
  `loginId` BIGINT NOT NULL,
  `loggedInTime` DATETIME NOT NULL,
  `loggedOutTime` DATETIME NULL,
  `createdAt` DATETIME NOT NULL,
  PRIMARY KEY (`loggedInfoId`));


--data
INSERT INTO login (username,password,blocked,incorrectTry,blockedTime,createdAt)
VALUES ('jan.kowalski','test1test',false,0,null,NOW());

INSERT INTO login (username,password,blocked,incorrectTry,blockedTime,createdAt)
VALUES ('rafal.polak','test1test',false,0,null,NOW());


INSERT INTO user (loginId, name, surname, address, email, phone, createdAt)
VALUES ((SELECT loginId FROM login WHERE username = 'rafal.polak'),
		'Rafał',
        'Polak',
        'Łódź',
        'test@test.com',
        '111222333',
        NOW());

INSERT INTO user (loginId, name, surname, address, email, phone, createdAt)
VALUES ((SELECT loginId FROM login WHERE username = 'jan.kowalski'),
		'Jan',
        'Kowalski',
        'Łódź',
        'test@test.com',
        '111222333',
        NOW());